# Importer service for alerts and silences from one Alertmanager service to another


The usecase is having on Alertmanager behind firewall, that needs to have also a subset of the alerts and silences that
another Alertmanager has.


# Flow
Some naming first:
DST_AM: destination Alertmanager instance, the one that needs to get alerts and silences imported to
SRC_AM: source Alertmanager instance, the one that exports the alerts and silences

## Alerts
### Alert existing in SRC_AM but not in DST_AM
The alert gets created in DST_AM with an extra label `{"source": "metricsinfra"}`.


### Alert existing in SRC_AM and in DST_AM
Nothing happens, note that the `source` label is ignored if it's value is `metricsinfra`.


### Alert does not exist in SRC_AM but it's in DST_AM
The alert gets deleted from DST_AM (only if it has the `{"source": "metricsinfra"}` label)


### Alert does not exist in SRC_AM and not in DST_AM
Nothing happens

## Silences
### Alert in SRC_AM is silenced
We find the silence that affects the alert that expires the latest, and we create a silence specific for that alert, that matches each label, no matter what the silence in SRC_AM is, expiring at that time, adding a matcher for the `source` label with `metricsinfra` label.


### Alert in SRC_AM gets it's silence removed
We remove any silence we created for it (by checking the matchers, including the one we created for the `source` label).


### Acked silences
It will create an ACKed silence (a silence starting with `ACK!`) if any of the silences of the original alerts is acked.
