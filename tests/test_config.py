from io import StringIO

import pytest

from amimporter.config import BadConfig, Config


class TestConfig:
    def test_from_yaml_works(self):
        yaml = StringIO(
            """
source_alertmanager_url: "http://dummy/src"
source_alertmanager_label: "metricsinfra"
source_alerts_label_selectors:
  - project: tools
  - project: toolsbeta
dest_alertmanager_url: "http://dummy/dest"
dest_team_label: "dummyteam"
        """
        )

        my_config = Config.from_yaml(yaml_buffer=yaml)

        assert my_config.source_alertmanager_url == "http://dummy/src"
        assert my_config.source_alertmanager_label == "metricsinfra"
        assert my_config.dest_alertmanager_url == "http://dummy/dest"
        assert my_config.dest_team_label == "dummyteam"
        assert my_config.source_alerts_label_selectors == [
            {"project": "tools"},
            {"project": "toolsbeta"},
        ]

    def test_from_yaml_raises_BadConfig_when_extra_config_passed(self):
        yaml = StringIO(
            """
source_alertmanager_url: "http://dummy/test"
dummy_entry: 1234
        """
        )

        with pytest.raises(BadConfig):
            Config.from_yaml(yaml_buffer=yaml)

    def test_from_yaml_raises_BadConfig_when_empty_config(self):
        yaml = StringIO("""""")

        with pytest.raises(BadConfig):
            Config.from_yaml(yaml_buffer=yaml)

    def test_from_yaml_raises_BadConfig_when_bad_yaml(self):
        yaml = StringIO("""I'm just not a yaml file.""")

        with pytest.raises(BadConfig):
            Config.from_yaml(yaml_buffer=yaml)
