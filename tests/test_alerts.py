import pytest

from amimporter.alerts import Alert, get_alerts_to_delete, get_new_alerts
from amimporter.config import Config

from .testlibs import get_dummy_alert


@pytest.mark.asyncio
async def test_get_new_alerts_adds_label_source(dummy_config: Config):
    expected_alert = get_dummy_alert()
    expected_alert["labels"]["source"] = dummy_config.source_alertmanager_label
    source_alerts = [get_dummy_alert()]

    gotten_alerts = get_new_alerts(source_alerts=source_alerts, dest_alerts=[], config=dummy_config)

    assert len(gotten_alerts) == 1
    assert gotten_alerts[0] == expected_alert


@pytest.mark.asyncio
async def test_get_new_alerts_skipps_adding_if_there_already(dummy_config: Config):
    source_alerts = [get_dummy_alert()]

    gotten_alerts = get_new_alerts(source_alerts=source_alerts, dest_alerts=source_alerts, config=dummy_config)

    assert gotten_alerts == []


@pytest.mark.asyncio
async def test_get_new_alerts_adds_if_there_already_with_superset_labels(dummy_config: Config):
    source_alerts = [get_dummy_alert()]
    dest_alerts = [get_dummy_alert(labels={**source_alerts[0]["labels"], **{"newlabel": "newvalue"}})]

    gotten_alerts = get_new_alerts(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert gotten_alerts == []


@pytest.mark.asyncio
async def test_get_new_alerts_skipps_adding_if_there_already_without_superset_labels(dummy_config: Config):
    source_alerts = [get_dummy_alert()]
    expected_alert = get_dummy_alert()
    expected_alert["labels"]["source"] = dummy_config.source_alertmanager_label
    dest_alerts = [get_dummy_alert(labels={})]

    gotten_alerts = get_new_alerts(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert len(gotten_alerts) == 1
    assert gotten_alerts[0] == expected_alert


@pytest.mark.asyncio
async def test_get_alerts_to_delete_returns_nothing_when_no_alerts(dummy_config: Config):
    source_alerts: list[Alert] = []
    dest_alerts: list[Alert] = []

    gotten_alerts = get_alerts_to_delete(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert gotten_alerts == []


@pytest.mark.asyncio
async def test_get_alerts_to_delete_returns_nothing_when_no_matching_alerts(dummy_config: Config):
    source_alerts: list[Alert] = []
    dest_alerts = [get_dummy_alert(labels={})]

    gotten_alerts = get_alerts_to_delete(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert gotten_alerts == []


@pytest.mark.asyncio
async def test_get_alerts_to_delete_returns_matching_alerts(dummy_config: Config):
    source_alerts: list[Alert] = []
    matching_alert = get_dummy_alert(
        labels={"source": dummy_config.source_alertmanager_label}, status={"state": "notresolved"}
    )
    dest_alerts = [matching_alert]
    expected_alerts = [matching_alert]

    gotten_alerts = get_alerts_to_delete(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert gotten_alerts == expected_alerts


@pytest.mark.asyncio
async def test_get_alerts_to_delete_returns_multiple_matching_alerts_and_filters_non_matching(dummy_config: Config):
    source_alerts: list[Alert] = []
    matching_alert_one = get_dummy_alert(
        labels={"source": dummy_config.source_alertmanager_label, "alertname": "alertone"},
        status={"state": "notresolved"},
    )
    matching_alert_two = get_dummy_alert(
        labels={"source": dummy_config.source_alertmanager_label, "alertname": "alerttwo"},
        status={"state": "notresolved"},
    )
    dest_alerts = [get_dummy_alert(), matching_alert_one, get_dummy_alert(), matching_alert_two]
    expected_alerts = [matching_alert_one, matching_alert_two]

    gotten_alerts = get_alerts_to_delete(source_alerts=source_alerts, dest_alerts=dest_alerts, config=dummy_config)

    assert gotten_alerts == expected_alerts
