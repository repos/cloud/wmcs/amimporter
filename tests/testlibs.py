from amimporter.alerts import Alert
from amimporter.config import Config

DEFAULT_SOURCE_LABEL = "sourceamlabel"


def get_dummy_config() -> Config:
    return Config(
        source_alertmanager_url="http://dummy/source/urc",
        source_alertmanager_label=DEFAULT_SOURCE_LABEL,
        source_proxy=None,
        source_alerts_label_selectors=[
            {"alertname": "PuppetStaleCertificates"},
        ],
        dest_alertmanager_url="http://dummy/dest/url",
        dest_team_label="teamlabel",
    )


def get_dummy_alert(**overrides) -> Alert:
    return {
        **{
            "annotations": {
                "runbook": "https://wikitech.wikimedia.org/wiki/Portal:Cloud_VPS/Admin/Runbooks/PuppetStaleCertificates",
                "summary": "Found non-revoked Puppet certificates for 1 deleted instances on tools-puppetmaster-02",
            },
            "endsAt": "2022-10-21T09:38:33.571Z",
            "fingerprint": "c52b2053e6d09cf2",
            "receivers": [{"name": "cloud-admin-feed"}],
            "startsAt": "2022-10-11T06:41:33.571Z",
            "status": {
                "inhibitedBy": [],
                "silencedBy": ["fa8c38d2-0857-4b83-9cd5-8a80d301fbcc"],
                "state": "suppressed",
            },
            "updatedAt": "2022-10-21T09:34:33.612Z",
            "generatorURL": "https://prometheus.wmflabs.org/...",
            "labels": {
                "alertname": "PuppetStaleCertificates",
                "instance": "tools-puppetmaster-02",
                "project": "tools",
                "severity": "warn",
            },
        },
        **overrides,
    }


def get_matching_alert(**overrides) -> Alert:
    overrides["labels"] = {**overrides.get("labels", {}), **{"source": DEFAULT_SOURCE_LABEL, "state": "notresolved"}}
    return get_dummy_alert(**overrides)


def get_dummy_silence(**overrides):
    return {
        **{
            "id": "d2c382be-c3ab-4607-b920-b9ce6de2c4e4",
            "matchers": [
                {"name": "alertname", "value": "PuppetAgentNoResources", "isRegex": False, "isEqual": True},
                {"name": "instance", "value": "deployment-etcd02", "isRegex": False, "isEqual": True},
                {"name": "job", "value": "node", "isRegex": False, "isEqual": True},
                {"name": "project", "value": "deployment-prep", "isRegex": False, "isEqual": True},
                {"name": "severity", "value": "warn", "isRegex": False, "isEqual": True},
            ],
            "startsAt": "2022-10-22T00:13:25.539999646Z",
            "endsAt": "2022-12-31T12:13:00Z",
            "updatedAt": "2022-10-22T00:13:25.539999646Z",
            "createdBy": "",
            "comment": "Dummy comment",
            "status": {"state": "active"},
        },
        **overrides,
    }
