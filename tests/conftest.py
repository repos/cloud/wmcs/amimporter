import pytest

from amimporter.config import Config

from .testlibs import get_dummy_config


@pytest.fixture
def dummy_config(**overrides) -> Config:
    return get_dummy_config(**overrides)
