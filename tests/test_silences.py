import copy
import re

import pytest

from amimporter.alerts import Alert
from amimporter.config import Config
from amimporter.silences import Silence, get_new_silences, get_silence_for_alert

from .testlibs import get_dummy_alert, get_dummy_silence


def assert_good_silence(silence: Silence, silenced_alert: Alert, source_label: str) -> None:
    assert re.match(
        r".*Automatically imported by amimporter on .*",
        silence["comment"],
    )

    for label_name, label_value in silenced_alert["labels"].items():
        matcher = {
            "name": label_name,
            "value": label_value,
            "isRegex": False,
            "isEqual": True,
        }
        assert matcher in silence["matchers"]

    source_matcher = {
        "name": "source",
        "value": source_label,
        "isRegex": False,
        "isEqual": True,
    }
    assert source_matcher in silence["matchers"]

    assert len(silence["matchers"]) == len(silenced_alert["labels"]) + 1


@pytest.mark.asyncio
async def test_get_new_silences_returns_nothing_if_no_alerts(dummy_config: Config):
    gotten_silences = get_new_silences(silenced_alerts=[], source_silences=[], dest_silences=[], config=dummy_config)

    assert gotten_silences == []


@pytest.mark.asyncio
async def test_get_new_silences_returns_nothing_if_non_silenced_alert_passed(dummy_config: Config):
    silenced_alerts = [get_dummy_alert()]
    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts, source_silences=[], dest_silences=[], config=dummy_config
    )

    assert gotten_silences == []


@pytest.mark.asyncio
async def test_get_new_silences_returns_new_silence_if_matching_silenced_alert(dummy_config: Config):
    expected_silence = get_dummy_silence()
    expected_silence["matchers"].append(
        {"name": "source", "value": dummy_config.source_alertmanager_label, "isRegex": False, "isEqual": True}
    )
    new_alert = get_dummy_alert()
    new_alert["status"]["state"] = "suppressed"
    new_alert["status"]["silencedBy"] = [expected_silence["id"]]
    source_silences = [get_dummy_silence()]

    silenced_alerts = [new_alert, get_dummy_alert(status={"state": "notsupressed"})]
    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts,
        source_silences=source_silences,
        dest_silences=[],
        config=dummy_config,
    )

    assert len(gotten_silences) == 1
    assert_good_silence(
        silence=gotten_silences[0], silenced_alert=new_alert, source_label=dummy_config.source_alertmanager_label
    )


@pytest.mark.asyncio
async def test_get_new_silences_returns_new_silence_with_latest_endsat(dummy_config: Config):
    expected_silence = get_dummy_silence()
    expected_silence["matchers"].append(
        {"name": "source", "value": dummy_config.source_alertmanager_label, "isRegex": False}
    )
    new_alert = get_dummy_alert()
    new_alert["status"]["state"] = "suppressed"
    new_alert["status"]["silencedBy"] = [expected_silence["id"]]
    expected_endsat = "2022-10-21T09:38:33.571Z"
    source_silences = [
        get_dummy_silence(endsAt="2022-10-19T09:38:33.571Z"),
        get_dummy_silence(endsAt=expected_endsat),
        get_dummy_silence(endsAt="2022-10-17T09:38:33.571Z"),
    ]

    silenced_alerts = [new_alert]
    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts,
        source_silences=source_silences,
        dest_silences=[],
        config=dummy_config,
    )

    assert len(gotten_silences) == 1
    assert_good_silence(
        silence=gotten_silences[0], silenced_alert=new_alert, source_label=dummy_config.source_alertmanager_label
    )
    assert gotten_silences[0]["endsAt"] == expected_endsat


@pytest.mark.asyncio
async def test_get_new_silences_returns_none_if_silence_already_exists_with_same_endsat(dummy_config: Config):
    new_alert = get_dummy_alert()
    new_alert["status"]["state"] = "suppressed"
    source_silences = [get_dummy_silence()]
    expected_silence = get_silence_for_alert(
        ends_at=source_silences[0]["endsAt"], alert=new_alert, alert_silences=[], config=dummy_config
    )
    expected_silence["id"] = "some-random-silence-id"
    new_alert["status"]["silencedBy"] = [expected_silence["id"]]
    dest_silences = [expected_silence]

    silenced_alerts = [new_alert, get_dummy_alert(status={"state": "notsupressed"})]
    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts,
        source_silences=source_silences,
        dest_silences=dest_silences,
        config=dummy_config,
    )

    assert len(gotten_silences) == 0


@pytest.mark.asyncio
async def test_get_new_silences_returns_a_new_silence_if_one_already_exists_with_earlier_endsat(dummy_config: Config):
    source_silences = [get_dummy_silence()]
    source_silences[0]["endsAt"] = "2022-10-21T09:38:33.571Z"

    new_alert = get_dummy_alert(suppressedBy=source_silences[0]["id"])
    new_alert["status"]["state"] = "suppressed"
    new_alert["status"]["silencedBy"] = [source_silences[0]["id"]]

    expected_silence = get_silence_for_alert(
        ends_at=source_silences[0]["endsAt"], alert=new_alert, alert_silences=[source_silences[0]], config=dummy_config
    )
    expected_silence["id"] = "some-random-silence-id"

    dest_silences = [copy.deepcopy(expected_silence)]
    dest_silences[0]["endsAt"] = source_silences[0]["endsAt"].replace("2022", "2021")
    silenced_alerts = [new_alert, get_dummy_alert(status={"state": "notsupressed"})]

    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts,
        source_silences=source_silences,
        dest_silences=dest_silences,
        config=dummy_config,
    )

    assert len(gotten_silences) == 1
    assert gotten_silences[0]["endsAt"] == expected_silence["endsAt"]
    assert gotten_silences[0]["matchers"] == expected_silence["matchers"]


@pytest.mark.asyncio
async def test_get_new_silences_returns_acked_silence_if_any_of_the_source_ones_is_acked(dummy_config: Config):
    source_silences = [get_dummy_silence(), get_dummy_silence(), get_dummy_silence()]
    source_silences[1]["comment"] = "ACK! some random comment :}"

    silenced_alerts = [get_dummy_alert(suppressedBy=source_silences[0]["id"])]
    silenced_alerts[0]["status"]["state"] = "suppressed"
    silenced_alerts[0]["status"]["silencedBy"] = [silence["id"] for silence in source_silences]

    gotten_silences = get_new_silences(
        silenced_alerts=silenced_alerts,
        source_silences=source_silences,
        dest_silences=[],
        config=dummy_config,
    )

    assert len(gotten_silences) == 1
    assert re.match(
        r"^ACK!.*" + source_silences[1]["comment"][4:] + r".*Automatically imported.*", gotten_silences[0]["comment"]
    )
