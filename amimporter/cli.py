import asyncio
import logging
from pathlib import Path

import click

from amimporter.config import Config
from amimporter.server import sync_all

LOGGER = logging.getLogger(__name__)


@click.command()
@click.option("-c", "--config-file", required=True, help="Path to the yaml config file", type=Path)
@click.option("-v", "--verbose", required=False, help="If set, will be way more verbose.", is_flag=True)
def main(config_file: Path, verbose: bool):

    logging.basicConfig(level=logging.DEBUG if verbose else logging.INFO)
    config = Config.from_yaml(yaml_buffer=config_file.open(mode="r", encoding="utf-8"))
    LOGGER.info("Starting AMImporter with config:\n%r", config)
    asyncio.run(sync_all(config=config))
