"""Alertmanager client methods."""
from __future__ import annotations

import asyncio
import datetime
import logging
from dataclasses import dataclass
from typing import Any

import aiohttp
from aiohttp_socks import ProxyConnector

from amimporter.alerts import Alert
from amimporter.silences import Silence


class AMCLientError(Exception):
    pass


class BadServerReply(AMCLientError):
    pass


@dataclass(frozen=True)
class AMClient:
    alertmanager_base_url: str
    dest_team_label: str
    proxy: None | str = None

    @property
    def session_ctx(self) -> aiohttp.ClientSession:
        if self.proxy:
            connector = ProxyConnector.from_url(self.proxy)
            return aiohttp.ClientSession(connector=connector)
        return aiohttp.ClientSession()

    async def _do_get_json(self, resource: str, data: dict[str, Any] = None) -> Any:
        data = {} if data is None else data
        async with self.session_ctx as session:
            async with session.get(
                f"{self.alertmanager_base_url}/api/v1/{resource}",
                headers={"Accept": "application/json"},
                data=data,
            ) as response:
                if response.status != 200:
                    raise BadServerReply(f"Got bad return code: {response}")
                try:
                    data = await response.json()
                except aiohttp.ClientResponseError as error:
                    raise BadServerReply(f"Unable to extract json from response: {response}") from error

        return data

    async def get_alerts(self) -> list[Alert]:
        alerts = await self._do_get_json(resource="alerts")
        return alerts["data"]

    async def get_silences(self, filter_str: str = "") -> list[Silence]:
        # it seems we can't filter out expired silences, we get all
        silences = await self._do_get_json(resource="silences", data={"filter": filter_str})
        return silences["data"]

    def to_postable_alert(self, alert: Alert) -> Alert:
        alert["labels"]["team"] = self.dest_team_label
        return alert

    async def create_alerts(self, alerts: list[Alert]) -> None:
        logging.info("Creating %d alerts in %s (proxy: %s)", len(alerts), self.alertmanager_base_url, self.proxy)
        to_post = [self.to_postable_alert(alert) for alert in alerts]
        async with self.session_ctx as session:
            async with session.post(f"{self.alertmanager_base_url}/api/v1/alerts", json=to_post) as response:
                if response.status != 200:
                    raise BadServerReply(f"Got bad return code: {await response.text()}")

    async def delete_alerts(self, alerts: list[Alert]) -> None:
        # deleting means setting the status as resolved
        logging.info("Deleting %d alerts in %s (proxy: %s)", len(alerts), self.alertmanager_base_url, self.proxy)
        to_post = [
            self.to_postable_alert(
                {**alert, **{"endsAt": f"{datetime.datetime.now(tz=datetime.timezone.utc).isoformat()}"}}
            )
            for alert in alerts
        ]
        async with self.session_ctx as session:
            async with session.post(f"{self.alertmanager_base_url}/api/v1/alerts", json=to_post) as response:
                if response.status != 200:
                    raise BadServerReply(f"Got bad return code: {await response.text()}")

    async def create_silences(self, silences: list[Silence]) -> None:
        logging.info("Creating %d silences in %s (proxy: %s)", len(silences), self.alertmanager_base_url, self.proxy)
        async with self.session_ctx as session:

            async def _create_silence(silence):
                async with session.post(f"{self.alertmanager_base_url}/api/v1/silences", json=silence) as response:
                    if response.status != 200:
                        raise BadServerReply(f"Got bad return code: {await response.text()}")

            await asyncio.gather(*[_create_silence(silence) for silence in silences])

    async def delete_silences(self, silence_ids: list[str]) -> None:
        logging.info("Expiring %d silences in %s (proxy: %s)", len(silence_ids), self.alertmanager_base_url, self.proxy)
        async with self.session_ctx as session:

            async def _delete_silence(silence_id):
                async with session.delete(f"{self.alertmanager_base_url}/api/v1/silence/{silence_id}") as response:
                    if response.status != 200:
                        raise BadServerReply(f"Got bad return code: {await response.text()}")

            await asyncio.gather(*[_delete_silence(silence_id) for silence_id in silence_ids])
