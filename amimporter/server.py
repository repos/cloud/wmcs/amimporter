from __future__ import annotations

import asyncio
import json
import logging
from itertools import chain

from amimporter.alerts import get_alerts_to_delete, get_alerts_to_keep, get_new_alerts
from amimporter.amclient import AMClient
from amimporter.config import Config
from amimporter.silences import get_new_silences, get_silences_to_delete

LOGGER = logging.getLogger(__name__)


async def sync_all(config: Config):
    source_am_client = AMClient(
        alertmanager_base_url=config.source_alertmanager_url,
        proxy=config.source_proxy,
        dest_team_label=config.dest_team_label,
    )
    dest_am_client = AMClient(
        alertmanager_base_url=config.dest_alertmanager_url,
        dest_team_label=config.dest_team_label,
    )

    cur_source_alerts, cur_dest_alerts, cur_source_silences, cur_dest_silences = await asyncio.gather(
        source_am_client.get_alerts(),
        dest_am_client.get_alerts(),
        source_am_client.get_silences(),
        dest_am_client.get_silences(filter_str=f"source={config.source_alertmanager_label}"),
    )

    selected_source_alerts = [
        alert
        for alert in cur_source_alerts
        if any(
            all(alert["labels"].get(label_name, "") == label_value for label_name, label_value in labelmatcher.items())
            for labelmatcher in config.source_alerts_label_selectors
        )
    ]

    LOGGER.debug("Got %d source alerts:\n%s", len(cur_source_alerts), json.dumps(cur_source_alerts, indent=4))
    LOGGER.debug("Got %d source silences:\n%s", len(cur_source_silences), json.dumps(cur_source_silences, indent=4))
    LOGGER.debug("Got %d dest alerts:\n%s", len(cur_dest_alerts), json.dumps(cur_dest_alerts, indent=4))
    LOGGER.debug("Got %d dest silences:\n%s", len(cur_dest_silences), json.dumps(cur_dest_silences, indent=4))

    new_alerts = get_new_alerts(
        source_alerts=selected_source_alerts,
        dest_alerts=cur_dest_alerts,
        config=config,
    )
    LOGGER.debug("Got %d new alerts:\n%s", len(new_alerts), json.dumps(new_alerts, indent=4))

    to_delete_alerts = get_alerts_to_delete(
        source_alerts=selected_source_alerts,
        dest_alerts=cur_dest_alerts,
        config=config,
    )
    LOGGER.debug("Got %d alerts to delete:\n%s", len(to_delete_alerts), json.dumps(to_delete_alerts, indent=4))

    to_keep_alerts = get_alerts_to_keep(
        source_alerts=selected_source_alerts,
        dest_alerts=cur_dest_alerts,
        config=config,
    )

    new_silences = get_new_silences(
        silenced_alerts=[
            alert for alert in chain(to_keep_alerts, new_alerts) if alert["status"]["state"] == "suppressed"
        ],
        source_silences=cur_source_silences,
        dest_silences=cur_dest_silences,
        config=config,
    )
    LOGGER.debug("Got %d new silences:\n%s", len(new_silences), json.dumps(new_silences, indent=4))

    LOGGER.info(
        (
            "Got %d source alerts, of which only %d passed the filters, %d source silences, %d dest alerts and "
            "%d dest silences\nFrom that, got %d new alerts, %d new silences and %d alerts to keep"
        ),
        len(cur_source_alerts),
        len(selected_source_alerts),
        len(cur_source_silences),
        len(cur_dest_alerts),
        len(cur_dest_silences),
        len(new_alerts),
        len(new_silences),
        len(to_keep_alerts),
    )

    to_expire_silences = get_silences_to_delete(
        new_silences=new_silences, dest_silences=cur_dest_silences, config=config
    )
    LOGGER.debug("Got %d silences to expire:\n%s", len(to_expire_silences), json.dumps(to_expire_silences, indent=4))

    # create new silences before new alerts to avoid paging
    # delete old alerts before old silences too, same reason
    await asyncio.gather(
        dest_am_client.create_silences(new_silences),
        dest_am_client.delete_alerts(to_delete_alerts),
    )

    await asyncio.gather(
        # we must refresh all the alerts, or they expire by themselves
        dest_am_client.create_alerts(new_alerts + to_keep_alerts),
        dest_am_client.delete_silences(to_expire_silences),
    )
