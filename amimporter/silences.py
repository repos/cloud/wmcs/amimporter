"""Silence specific function"""
from __future__ import annotations

import datetime
import logging
from typing import Any, cast

from amimporter.alerts import Alert, AlertStates
from amimporter.config import Config

Silence = dict[str, Any]
LOGGER = logging.getLogger(__name__)


def get_silences_to_delete(new_silences: list[Alert], dest_silences: list[Alert], config: Config) -> list[str]:
    amimporter_match = get_source_matcher(config)
    return [
        silence["id"]
        for silence in dest_silences
        if amimporter_match not in silence["matchers"]
        and silence["status"]["state"] != "expired"
        and _get_matching_silence(silence, new_silences, config=config) is None
    ]


def get_source_matcher(config: Config):
    return {
        "name": "source",
        "value": config.source_alertmanager_label,
        "isRegex": False,
        "isEqual": True,
    }


def normalize_date(date_str: str) -> str:
    # strip out the nanoseconds, python datetime does not support it and we don't mind if a silence is <1s shorter
    # than it should
    if "." in date_str:
        date_str = date_str.split(".", 1)[0]
    if date_str.endswith("Z"):
        date_str = date_str[:-1]
    return date_str


def is_older(date_one: str, date_two: str) -> bool:
    return datetime.datetime.fromisoformat(normalize_date(date_one)) > datetime.datetime.fromisoformat(
        normalize_date(date_two)
    )


def _get_matching_silence(silence: Silence, silences: list[Silence], config: Config) -> Silence | None:
    for maybe_match in silences:
        # skip the extra source label match that we add to know it's created by us
        maybe_match_matches = [
            match
            for match in maybe_match["matchers"]
            if match["name"] != "source" or match["value"] != config.source_alertmanager_label
        ]
        silence_matches = [
            match
            for match in silence["matchers"]
            if match["name"] != "source" or match["value"] != config.source_alertmanager_label
        ]
        if silence_matches == maybe_match_matches and not is_older(silence["endsAt"], maybe_match["endsAt"]):
            return silence
    return None


def _get_latest_end_at(silences: list[Silence]) -> None | str:
    if not silences:
        return None

    return sorted(silences, key=lambda silence: datetime.datetime.fromisoformat(normalize_date(silence["endsAt"])))[-1][
        "endsAt"
    ]


def _get_aggregated_comment(silences: None | list[Silence]) -> str:
    base_comment = f"Automatically imported by amimporter on {datetime.datetime.now()}"
    if not silences:
        return base_comment

    aggregated_comments = "|".join(
        silence["comment"][4:] if silence["comment"].startswith("ACK!") else silence["comment"] for silence in silences
    )
    if any(silence["comment"].startswith("ACK!") for silence in silences):
        aggregated_comments = "ACK!" + aggregated_comments

    return f"{aggregated_comments}|{base_comment}"


def get_new_silences(
    silenced_alerts: list[Alert],
    source_silences: list[Silence],
    dest_silences: list[Silence],
    config: Config,
) -> list[Silence]:
    silences: list[Silence] = []
    for alert in silenced_alerts:
        if alert["status"]["state"] != AlertStates.SUPPRESSED.value:
            LOGGER.warning("Got a non-silenced alert: %s", str(alert))
            continue

        alert_silences = [silence for silence in source_silences if silence["id"] in alert["status"]["silencedBy"]]
        source_latest_end_at = _get_latest_end_at(alert_silences)
        if not alert_silences or not source_latest_end_at:
            continue

        maybe_new_silence = get_silence_for_alert(
            ends_at=source_latest_end_at, alert=alert, alert_silences=alert_silences, config=config
        )
        existing_silence = _get_matching_silence(maybe_new_silence, dest_silences, config=config)

        if existing_silence is None:
            silences.append(maybe_new_silence)
        else:
            latest_ends_at = _get_latest_end_at([existing_silence, maybe_new_silence])
            if existing_silence["endsAt"] != latest_ends_at:
                # extends existing silence
                existing_silence["endsAt"] = latest_ends_at
                silences.append(maybe_new_silence)

    return silences


def get_silence_for_alert(ends_at: str, alert: Alert, alert_silences: list[Alert], config: Config) -> Silence:
    new_silence = {
        "endsAt": ends_at,
        "matchers": [
            {
                "name": label_name,
                "value": label_value,
                "isRegex": False,
                "isEqual": True,
            }
            for label_name, label_value in alert["labels"].items()
        ],
        "comment": _get_aggregated_comment(silences=alert_silences),
    }
    if "source" not in alert["labels"]:
        cast(list[dict[str, Any]], new_silence["matchers"]).append(get_source_matcher(config))
    return new_silence
