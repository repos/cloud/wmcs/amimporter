"""Config related methods"""
from dataclasses import dataclass
from typing import TextIO

import yaml


class BadConfig(Exception):
    """Raised whet the config is not valid."""


@dataclass(frozen=True)
class Config:
    source_alertmanager_url: str
    source_alertmanager_label: str
    source_alerts_label_selectors: list[dict[str, str]]
    dest_alertmanager_url: str
    dest_team_label: str
    source_proxy: None | str = None

    @classmethod
    def from_yaml(cls, yaml_buffer: TextIO) -> "Config":
        try:
            yaml_obj = yaml.load(stream=yaml_buffer, Loader=yaml.SafeLoader)
            return cls(**yaml_obj)
        except Exception as error:
            raise BadConfig(f"There was an error when parsing the config: {error}") from error
