"""Functions related to alerts."""
from __future__ import annotations

import copy
from enum import Enum
from typing import Any

from amimporter.config import Config

Alert = dict[str, Any]


class AlertStates(Enum):
    SUPPRESSED = "suppressed"
    ACTIVE = "active"
    UNPROCESSED = "unprocessed"


def _get_matching_alert(which_alert: Alert, in_alerts: list[Alert]) -> Alert | None:
    for alert in in_alerts:
        if set(which_alert["labels"]).issubset(set(alert["labels"])):
            return alert

    return None


def get_new_alerts(source_alerts: list[Alert], dest_alerts: list[Alert], config: Config) -> list[Alert]:
    return [
        {**copy.deepcopy(alert), **{"labels": {**alert["labels"], **{"source": config.source_alertmanager_label}}}}
        for alert in source_alerts
        if _get_matching_alert(which_alert=alert, in_alerts=dest_alerts) is None
    ]


def get_alerts_to_delete(source_alerts: list[Alert], dest_alerts: list[Alert], config: Config) -> list[Alert]:
    return [
        {**alert}
        for alert in dest_alerts
        if _get_matching_alert(which_alert=alert, in_alerts=source_alerts) is None
        and alert["status"]["state"] != "resolved"
        and alert["labels"].get("source", "") == config.source_alertmanager_label
    ]


def get_alerts_to_keep(source_alerts: list[Alert], dest_alerts: list[Alert], config: Config) -> list[Alert]:
    return [
        {**alert}
        for alert in dest_alerts
        if (
            _get_matching_alert(which_alert=alert, in_alerts=source_alerts) is not None
            and alert["status"]["state"] != "resolved"
            and alert["labels"].get("source", "") == config.source_alertmanager_label
        )
    ]
